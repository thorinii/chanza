import signal
import sys
import threading
import time

import chanza.backup
import chanza.buildstamp
import chanza.config
import chanza.database
import chanza.email
import chanza.server

# TODO:
# - rebuild dashboard
#     - user stats (# of texts, # to practice)
#     - user global progress bar
#     - tabs: finished, learning (marked for familiarity), unstarted/shelved
#     - card for each text
#         - name, line/chunk count, % learnt
#         - learning coverage bar
#         - learn next button
# - Text Controls
#     - normalise indent to multiples of tabs (that get rendered as ~2 spaces)
# - sort text names accounting for numbers
#     - this may mean an internal ID that has parsed the numbers and padded them
# - Text Page rebuild
#     - show text created_at, updated_at (already added to schema)
#     - ability to delete texts
#     - if line count is the same, preserve chunks that haven't changed
#         - use proper diff system, and show consequences before actually saving
#     - separate text view from text edit
#         - show text as linear with associated chunk info down side
#     - option to choose chunk sequencing including custom
#     - add extra notes eg of authorship and other things to poems
# - backups
#     - RPi task to download them
# - text tags/categories and filtering
# - EXPERIMENT to figure out what rates of learn and review are sustainable
# - measure scheduling by day boundaries (using TZ); ignore hours
# - add log of things practiced so that we can pick one to think about
# - add a linear chunk sequence number for display (or line range)
# - better phrasing splitter tools based on binding power, prefix punct/suffix punct,
#     weak words (like conjuctions). Basically: split at every level, then follow
#     balancing AND precedence to join back together. Do this by building a tree
#     and balance-merge by length.
# - proper logging system
# - MAYBE(probably don't actually want it) show more than one chunk in learning email (1 either side if possible)


def main():
    signal.signal(signal.SIGINT, call_exit)
    signal.signal(signal.SIGTERM, call_exit)

    config = chanza.config.load_config()
    db = chanza.database.open_connection(config.db_path)

    threading.Thread(
        target=run_timer_thread,
        daemon=True,
        name="TimerThread",
        kwargs={"config": config},
    ).start()

    threading.Thread(
        target=run_backup_thread,
        daemon=True,
        name="BackupThread",
        kwargs={"config": config},
    ).start()

    print(
        f"Chanza {chanza.buildstamp.commit_sha}-{chanza.buildstamp.build_type} "
        f"({chanza.buildstamp.built_at}) ready"
    )

    chanza.server.start_server(db)
    print("Exiting...")
    return 0


def call_exit(signal_number, frame):
    print("Shutting down...")
    sys.exit(0)


def run_timer_thread(config: chanza.config.Config):
    CYCLE_TIME = 10 * 60  # 10 minutes

    print("TimerThread started", flush=True)
    while True:
        try:
            db = chanza.database.open_connection(config.db_path)
            with db:
                print("Refreshing recall probabilities...", flush=True)
                db.refresh_probabilities()
                print("Refreshed recall probabilities.", flush=True)

            chanza.email.update_reports(db, config)

            db = None
        except Exception as e:
            print("crash in TimerThread:", e)

        time.sleep(CYCLE_TIME)


def run_backup_thread(config: chanza.config.Config):
    CYCLE_TIME = 12 * 60 * 60  # 12h

    print("BackupThread started", flush=True)
    while True:
        try:
            db = chanza.database.open_connection(config.db_path)
            filename = chanza.backup.make_backup(config, db)
            print(f"Backed up to {filename}")

            pruned = chanza.backup.prune_backups(config)
            print(f"Pruned {pruned} backups")

            db = None
        except Exception as e:
            print("crash in BackupThread:", e)

        time.sleep(CYCLE_TIME)


if __name__ == "__main__":
    sys.exit(main())
