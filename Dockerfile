FROM python:3.12-slim
WORKDIR /app

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt \
 && groupadd --gid 10400 chanza \
 && useradd --uid 10400 chanza -g chanza

COPY main.py /app/
COPY chanza/* /app/chanza/


USER chanza
VOLUME /var/chanza \
       /var/log
ENV DATA_PATH=/var/chanza \
    BACKUP_PATH=/var/chanza/backups \
    BACKUP_COUNT=10 \
    LOG_PATH=/var/log/chanza.log \
    SMTP_SERVER=localhost:25 \
    FROM_ADDRESS= \
    WEB_ADDRESS=

ENV CHANZA_BUILD_TYPE=release \
    PYTHONUNBUFFERED=1

EXPOSE 3000
CMD ["python", "/app/main.py"]
