from datetime import datetime, timedelta
from email.message import EmailMessage
import html
import smtplib
import urllib.parse

import chanza.buildstamp
from chanza.config import Config
from chanza.database import Database, User
from chanza.data import get_now


def update_reports(db: Database, config: Config):
    for user in db.list_users():
        if not user.email:
            continue
        update_report(db, config, user)


def update_report(db: Database, config: Config, user: User):
    now_utc = get_now()
    now_local = now_utc.astimezone(user.timezone)
    # aim to send at 6am
    time_to_send = now_local.replace(hour=6, minute=0, second=0, microsecond=0)

    already_sent = user.last_reported_at and user.last_reported_at >= time_to_send
    if now_local < time_to_send or already_sent:
        return

    send_report(db, config, user, now_local)
    with db:
        db.update_user(
            id=user.id,
            email=user.email,
            timezone=user.timezone,
            last_reported_at=now_utc,
        )


def send_report(db: Database, config: Config, user: User, now: datetime):
    print(f"Sending report for {user.username}...")
    report = format_report(db, config, now)
    if report:
        if chanza.buildstamp.debug:
            print("Sent report (dry-run).")
        else:
            send_email(config, user, report)
            print("Sent report.")
    else:
        print("No report necessary.")


def format_report(db: Database, config: Config, now: datetime):
    chunk = db.get_weakest_chunk(1)
    if not chunk:
        return None
    text = db.get_text(1, chunk.text_id)

    lines = chunk.slice.select(text.text)
    leading_lines = html.escape("\n".join(lines[:2])).rstrip()
    trailing_lines = html.escape("\n".join(lines[-1:])).rstrip()

    def format_link(url: str, text: str) -> str:
        return f'<a href="{html.escape(url)}">{html.escape(text)}</a>'

    submission_url = urllib.parse.urljoin(
        base=config.web_address, url=f"practice/{chunk.id}"
    )
    practice_section = f"""
        <h2>Practice</h2>
        <p>
        Start from:
        <pre>{leading_lines}</pre>
        <br>
        And go to:
        <pre>{trailing_lines}</pre>
        <br>
        {format_link(submission_url, 'Submit results')}
        </p>
        """
    header = "<h1>Chanza Daily Practice Report</h1>"
    body = "".join([practice_section])
    footer = f"<p><small>Generated {html.escape(now.isoformat('T'))}</small></p>"
    return "\n".join([header, body, footer])


def send_email(config: Config, user: User, message: str) -> None:
    msg = EmailMessage()
    msg.set_content(message, subtype="html")

    msg["Subject"] = "Chanza Daily Practice Report"
    msg["From"] = config.from_address
    msg["To"] = user.email

    s = smtplib.SMTP(host=config.smtp_server[0], port=config.smtp_server[1])
    s.send_message(msg)
    s.quit()
