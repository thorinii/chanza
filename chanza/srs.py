from datetime import datetime, timedelta
from typing import NamedTuple, Tuple
import ebisu

from chanza.data import get_now

TIME_UNIT = timedelta(days=1)


class SrsState(NamedTuple):
    """
    The Spaced-Repetition System state of a single chunk. This is the stuff
    necessary for the Ebisu library, which uses a Bayesian beta distribution.

    Alpha and Beta are equal, and are the confidence that the half-life matches
    what the user's memory experience will be. These default to 3.

    T is the half-life of the memory.
    """

    ebisu: Tuple[float, float, float]
    recall_probability: float
    last_practiced: datetime


def generate_initial_srs_state(halflife=timedelta(days=0.5)) -> SrsState:
    return SrsState(
        ebisu=(3, 3, halflife / TIME_UNIT),
        recall_probability=1.0,
        last_practiced=get_now(),
    )


def update_state(old_state: SrsState, good: bool) -> SrsState:
    now = get_now()

    return SrsState(
        ebisu=ebisu.updateRecall(
            prior=old_state.ebisu,
            successes=1 if good else 0,
            total=1,
            tnow=(now - old_state.last_practiced) / TIME_UNIT,
        ),
        recall_probability=1.0,
        last_practiced=now,
    )


def refresh_probability(state: SrsState) -> float:
    """
    Note that this doesn't return a [0,1) probability; it returns a sortable
    number that "behaves like" a probability.
    """

    now = get_now()
    return ebisu.predictRecall(
        prior=state.ebisu,
        tnow=(now - state.last_practiced) / TIME_UNIT,
    )


def calculate_accurate_probability(state: SrsState) -> float:
    """
    Note that this doesn't return a [0,1) probability; it returns a sortable
    number that "behaves like" a probability.
    """

    now = get_now()
    return ebisu.predictRecall(
        prior=state.ebisu,
        tnow=(now - state.last_practiced) / TIME_UNIT,
        exact=True,
    )
