from typing import NamedTuple, Tuple
import os
import smtplib


class Config(NamedTuple):
    db_path: str
    backup_path: str
    number_of_backups: int

    smtp_server: Tuple[str, int]
    from_address: str

    web_address: str


def load_config() -> Config:
    if "SMTP_SERVER" in os.environ:
        host, port = os.environ["SMTP_SERVER"].split(":")
        smtp_server = (host, int(port))
    else:
        smtp_server = ("localhost", smtplib.SMTP_PORT)

    return Config(
        db_path=os.environ.get("DATA_PATH", "."),
        backup_path=os.environ.get("BACKUP_PATH", "backups"),
        number_of_backups=int(os.environ.get("BACKUP_COUNT", "2")),
        smtp_server=smtp_server,
        from_address=os.environ.get("FROM_ADDRESS", "chanza@example.com"),
        web_address=os.environ.get("WEB_ADDRESS", "http://localhost:3000"),
    )
