from datetime import datetime, timezone
from typing import Dict, Iterable, List, NamedTuple
import itertools
import math
import re


class ParsedText(NamedTuple):
    lines: List[str]

    @classmethod
    def parse(cls, raw: str) -> "ParsedText":
        """
        Breaks a raw string up into a list of content lines with any extra
        line breaks tacked onto the end of each line. This means indexing into
        the list will always come up with a content line.
        """
        raw = raw.replace("\r\n", "\n").replace("\r", "").rstrip()

        regex = re.compile("([^\n]+)(\n*)")
        lines = [
            match[1].rstrip() + ("\n" * max(0, len(match[2]) - 1))
            for match in regex.finditer(raw)
        ]

        return ParsedText(lines)

    def __str__(self):
        return "\n".join(self.lines)


class Slice(NamedTuple):
    first: int
    last: int

    def select(self, text: ParsedText) -> List[str]:
        return text.lines[self.first : self.last + 1]

    def intersect(self, other: "Slice") -> "Slice":
        return Slice(max(self.first, other.first), min(self.last, other.last))

    def intersects(self, other: "Slice") -> bool:
        return self.first <= other.last and self.last >= other.first

    def union(self, other: "Slice") -> "Slice":
        if not self.intersects(other):
            raise ValueError(f"Slices do not overlap: {self} and {other}")
        return Slice(min(self.first, other.first), max(self.last, other.last))


def divide_chunks(text: ParsedText, lines_per_chunk: int, overlap: int) -> List[Slice]:
    if lines_per_chunk > 0:
        return divide_chunks_by_count(text, lines_per_chunk, overlap)
    else:
        return divide_chunks_by_blank(text, overlap)


def divide_chunks_by_count(
    text: ParsedText, lines_per_chunk: int, overlap: int
) -> List[Slice]:
    """
    Slices a series of lines into chunks, not counting stanza breaks / empty lines.
    """

    # figure out which line each chunk should start on
    chunk_spacing = lines_per_chunk - overlap
    chunk_start_indexes = list(range(0, len(text.lines), chunk_spacing))

    # remove the last chunk if redundant
    if chunk_start_indexes[-2] + lines_per_chunk >= len(text.lines):
        chunk_start_indexes.pop()

    # make chunk slices for each chunk using the original line numbers
    return [
        Slice(index, min(index + lines_per_chunk - 1, len(text.lines) - 1))
        for index in chunk_start_indexes
    ]


def divide_chunks_by_blank(text: ParsedText, overlap: int) -> List[Slice]:
    """
    Slices a series of lines into chunks based on whitespace.
    """

    # find indexes of lines that end a chunk
    chunk_ends = [i for i, line in enumerate(text.lines) if line.endswith("\n")]
    # include first and last as endpoints
    chunk_ends.insert(0, -1)
    chunk_ends.append(len(text.lines) - 1)

    # make list of the chunks before overlapping
    chunk_pairs = list(zip((i + 1 for i in chunk_ends), chunk_ends[1:]))

    # drop last chunk if the previous one reaches just as far
    if chunk_pairs[-2][1] + overlap >= chunk_pairs[-1][1]:
        chunk_pairs.pop()

    # make chunk slices for each chunk using the original line numbers
    return [
        Slice(start, min(len(text.lines) - 1, end + overlap))
        for start, end in chunk_pairs
    ]


def sequence_packets(start: int, end: int) -> Iterable[int]:
    """
    A function to sequence packets in a binary search order. Eg:
        f(1, 9) = 1 5 3 7 2 6 4 8 9
    """

    if start == end:
        yield start
    elif start + 1 == end:
        yield start
        yield end
    else:
        midpoint = (end - start + 1) // 2 + start
        for left, right in itertools.zip_longest(
            sequence_packets(start, midpoint - 1), sequence_packets(midpoint, end)
        ):
            if left is not None:
                yield left
            if right is not None:
                yield right


def sequence_chunks(count: int, packet_length: int = None) -> Iterable[int]:
    """
    Divy chunks into packets, sequence the packets by binary partitioning,
    and learn each one completely before moving on. Eg with a length of 9
    chunks per packet:
        1..................2.........................
        @1.................@2........................
        <snip>
        @@@@@@@@1..........@@@@@@@@2.................
        @@@@@@@@@..1.......@@@@@@@@@.....2...........
        @@@@@@@@@..@1......@@@@@@@@@.....@2..........
        <snip>
    """

    if not packet_length:
        packet_length = 5

    # first sequence pairs of chunks
    num_packets = math.ceil(count / packet_length)
    for packet in sequence_packets(0, num_packets - 1):
        for i in range(packet_length):
            if packet * packet_length + i < count:
                yield packet * packet_length + i


def get_now() -> datetime:
    return datetime.now(timezone.utc)
