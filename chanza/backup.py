from datetime import datetime, timezone
import gzip
import os
import tempfile

from chanza.config import Config
from chanza.database import Database

PREFIX = "chanza-backup-"
EXTENSION = ".sql.gz"


def make_backup(config: Config, db: Database) -> str:
    # ensure dir exists
    os.makedirs(config.backup_path, exist_ok=True)

    fd, tmp_path = tempfile.mkstemp(
        dir=config.backup_path, suffix=EXTENSION, text=False
    )
    try:
        with os.fdopen(fd, "wb") as raw_fp, gzip.open(raw_fp, "wb") as fp, db:
            db.backup_to(fp)

        final_path = os.path.join(config.backup_path, make_name())
        os.rename(tmp_path, final_path)
        return final_path
    except Exception:
        os.remove(tmp_path)
        raise


def make_name() -> str:
    now = datetime.now(timezone.utc)
    return PREFIX + now.strftime("%Y-%m-%d-%H%M%S") + EXTENSION


def prune_backups(config: Config) -> int:
    backup_paths = [
        x
        for x in os.listdir(config.backup_path)
        if x.startswith(PREFIX) and x.endswith(EXTENSION)
    ]
    backup_paths.sort()

    number_to_remove = len(backup_paths) - config.number_of_backups
    if number_to_remove <= 0:
        return 0

    for path in backup_paths[:number_to_remove]:
        os.remove(os.path.join(config.backup_path, path))
        print("Pruned backup", path)

    return number_to_remove
