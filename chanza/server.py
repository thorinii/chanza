import base64
import functools
import json
import os.path
from typing import Iterable
from zoneinfo import ZoneInfo, ZoneInfoNotFoundError

import bottle

import chanza.buildstamp
import chanza.database
from chanza.data import divide_chunks, sequence_chunks, ParsedText, Slice
import chanza.srs

DB: chanza.database.Database = None


def start_server(db: chanza.database.Database):
    global DB
    DB = db
    bottle.run(host="0.0.0.0", port=3000)


def authorised(required=True):
    def decorator(fn):
        @functools.wraps(fn)
        def wrapper(*args, **kwargs):
            if "Authorization" in bottle.request.headers:
                user = load_user_from_header(bottle.request.headers["Authorization"])
            elif chanza.buildstamp.debug:
                user = DB.get_user("lachlan")
            else:
                user = None
            if required and not user:
                return page_template(None, "403 Not Logged In", "<h1>Forbidden</h1>\n")
            return fn(*args, **kwargs, user=user)

        return wrapper

    return decorator


@bottle.route("/")
@authorised(required=False)
def main_page(user: chanza.database.User):
    if not user:
        return page_template(
            None, "403 Not Logged In", "<h1>You need to sign in</h1>\n"
        )

    texts_summary = DB.get_texts_summary(user.id)
    return page_template(
        user,
        "",
        """
        <h1>Texts</h1>
        <table>
            <thead>
            <tr>
                <td>Name</td>
                <td>Line Count</td>
                <td>Chunks (circulating/total)</td>
                <td>Chunk Length</td>
                <td>Familiarity Active?</td>
            </tr>
            </thead>

            {{!rows}}
        </table>
        """,
        rows="\n".join(
            [
                format(
                    """
                <tr>
                    <td><a href="/edit/{{id}}">{{name}}</a></td>
                    <td>{{line_count}}</td>
                    <td>{{known_count}}/{{chunk_count}}{{!learn_next_link}}</td>
                    <td>{{chunk_length}}</td>
                    <td>
                        <form method="POST" action="/text/familiarity/{{id}}">
                            <input type="hidden" name="next" value="{{inverted_familiarity}}">
                            <input type="submit" name="familiarity" value="{{familiarity_state}}">
                        </form>
                    </td>
                </tr>
                """,
                    id=text.id,
                    name=text.name,
                    line_count=text.line_count,
                    chunk_count=text.chunk_count,
                    known_count=text.circulating_chunk_count,
                    learn_next_link=(
                        ""
                        if text.chunk_count == text.circulating_chunk_count
                        else f' <a href="/learn/next/{text.id}">Learn Next</a>'
                    ),
                    chunk_length=text.chunk_length
                    if text.chunk_length > 0
                    else "stanza",
                    inverted_familiarity="1" if not text.familiarity_active else "0",
                    familiarity_state="On" if text.familiarity_active else "Off",
                )
                for text in texts_summary
            ]
        ),
    )


@bottle.route("/text/familiarity/<id>", method="post")
@authorised()
def save_text_familiarity_action(user: chanza.database.User, id):
    params = bottle.request.forms.decode()
    next_state: bool = params.get("next") != "0"
    print(id, params.get("next"), next_state)

    with DB:
        DB.update_text_familiarity(
            id=id,
            user_id=user.id,
            familiarity_active=next_state,
        )
    return bottle.redirect("/")


@bottle.route("/me")
@authorised()
def me_page(user: chanza.database.User):
    return page_template(
        user,
        format("{{name}}'s Profile", name=user.username),
        """
        <h1>{{name}}'s Profile</h1>
        <form method="post" action="/me/go" autocomplete="off">
            <label>
                Email address:
                <input type="email" name="email" placeholder="Email address" value="{{email}}">
            </label>
            <br>
            <label>
                Timezone:
                <input type="text" name="timezone" placeholder="Australia/Adelaide" value="{{timezone}}" required>
            </label>
            <br>
            <label>
                Last report:
                {{last_reported_at}}
            </label>
            <br>
            <input type="submit" name="submit" value="Save">
        </form>
        """,
        name=user.username,
        email=user.email if user.email else "",
        timezone=user.timezone.key,
        last_reported_at=(
            user.last_reported_at.astimezone(user.timezone).strftime("%Y-%m-%d %I:%M%p")
            if user.last_reported_at
            else "never"
        ),
    )


@bottle.route("/me/go", method="post")
@authorised()
def me_page_action(user: chanza.database.User):
    params = bottle.request.forms.decode()
    email: str = params.get("email")
    timezone_raw: str = params.get("timezone")

    email = email.strip()

    try:
        timezone = ZoneInfo(timezone_raw.strip())
    except ZoneInfoNotFoundError:
        return page_template(
            user,
            "Bad Timezone - User Edit",
            """
            <h1>Timezone {{timezone}} is not valid</h1>
            """,
            timezone=timezone_raw,
        )

    with DB:
        DB.update_user(
            id=user.id,
            email=email,
            timezone=timezone,
            last_reported_at=user.last_reported_at,
        )
    return bottle.redirect("/me")


@bottle.route("/practice/next")
@authorised()
def practice_next_redirect(user: chanza.database.User):
    with DB:
        chunk = DB.get_weakest_chunk(user.id)
        if not chunk:
            return bottle.redirect("/")
        return bottle.redirect(f"/practice/{chunk.id}")


@bottle.route("/add")
@authorised()
def add_page(user: chanza.database.User):
    return page_template(
        user,
        "Add Text",
        """
        <h1>Add</h1>
        <form method="post" action="/add/go" autocomplete="off">
            <label>
                Text Name:
                <input type="text" name="name" placeholder="Name" required>
            </label>
            <br>
            <label>
                Chunk length (zero to split at blank lines):
                <input type="number" name="chunk_length" step="1" value="12" required>
            </label>
            <br>
            <button id="btn-join">Join Lines</button>
            <button id="btn-split-punctuation">Split on punctuation</button>
            <br>
            <label>
                Content:
                <textarea name="content" required class="raw_text"></textarea>
            </label>
            <br>
            <input type="submit" name="submit" value="Add">
        </form>
        """,
    )


@bottle.route("/add/go", method="post")
@authorised()
def add_page_action(user: chanza.database.User):
    params = bottle.request.forms.decode()
    name: str = params.get("name")

    try:
        chunk_length = int(params.get("chunk_length"))
        if chunk_length < 0:
            chunk_length = 12
    except ValueError:
        chunk_length = 12

    content: str = params.get("content")
    text = ParsedText.parse(content)

    chunks = divide_chunks(text, chunk_length, overlap=2)
    chunk_sequence = sequence_chunks(len(chunks))

    with DB:
        DB.save_text(
            user_id=user.id,
            name=name,
            text=text,
            chunk_length=chunk_length,
            chunks=chunks,
            sequence=chunk_sequence,
        )
    return bottle.redirect("/")


@bottle.route("/edit/<id>")
@authorised()
def edit_page(user: chanza.database.User, id):
    text = DB.get_text(user.id, id)
    chunks = list(DB.get_chunks_for_text(user.id, id))
    if not text:
        return page_template(
            user,
            "404 Text Edit",
            """
            <h1>Text {{id}} not found</h1>
            """,
            id=id,
        )

    return page_template(
        user,
        "Edit " + text.name,
        """
        <h1>Text &mdash; {{name}}</h1>
        <form method="post" action="/edit/go/{{id}}" autocomplete="off">
            <label>
                Text Name:
                <input type="text" name="name" placeholder="Name" required value="{{name}}">
            </label>
            <br>
            <label>
                Chunk length (zero to split at blank lines):
                <input type="number" name="chunk_length" step="1" value="{{chunk_length}}" required>
            </label>
            <br>
            Line count: {{line_count}}
            <br>
            <button id="btn-join">Join Lines</button>
            <button id="btn-split-punctuation">Split on punctuation</button>
            <br>
            <label>
                Content:
                <textarea name="content" required class="raw_text">{{raw_text}}</textarea>
            </label>
            <br>
            <input type="submit" name="submit" value="Save">
        </form>

        <table>
            <thead><tr>
                <td>Lines</td>
                <td>Sample</td>
                <td>Controls</td>
            </tr></thead>
            {{!chunks}}
        </table>
        """,
        id=text.id,
        name=text.name,
        chunk_length=text.chunk_length,
        line_count=text.line_count,
        raw_text=str(text.text),
        chunks="\n".join(
            format(
                '<tr><td>{{range}}</td><td><div class="formatted-text">{{sample}}</div></td><td>{{!controls}}</td></tr>',
                range=f"{c.slice.first+1} - {c.slice.last+1}",
                sample="\n".join(
                    [l for l in c.slice.select(text.text) if l][0:2]
                ).strip(),
                controls=(
                    (
                        f'<form method="post" action="/edit/chunk/{c.id}">'
                        '<input type="submit" name="submit" value="Reset">'
                        "</form>"
                    )
                    if c.srs_state
                    else f'<a href="/learn/{c.id}">Learn</a>'
                ),
            )
            for c in chunks
        ),
    )


@bottle.route("/edit/go/<id>", method="post")
@authorised()
def edit_page_action(user: chanza.database.User, id):
    params = bottle.request.forms.decode()
    name: str = params.get("name")

    try:
        chunk_length = int(params.get("chunk_length"))
        if chunk_length < 0:
            chunk_length = 12
    except ValueError:
        chunk_length = 12

    content: str = params.get("content")
    text = ParsedText.parse(content)

    chunks = divide_chunks(text, chunk_length, overlap=2)
    chunk_sequence = sequence_chunks(len(chunks))

    with DB:
        DB.update_text(
            id=id,
            user_id=user.id,
            name=name,
            text=text,
            chunk_length=chunk_length,
            chunks=chunks,
            sequence=chunk_sequence,
        )
    return bottle.redirect(f"/edit/{id}")


@bottle.route("/edit/chunk/<id>", method="post")
@authorised()
def edit_chunk_action(user: chanza.database.User, id):
    params = bottle.request.forms.decode()
    submit_action: str = params.get("submit")

    with DB:
        if submit_action == "Reset":
            DB.save_chunk_srs(user.id, id, None)
        else:
            raise Exception(f"unknown chunk edit action: {submit_action}")

        chunk = DB.get_chunk(user.id, id)
    return bottle.redirect(f"/edit/{chunk.text_id}")


@bottle.route("/learn/next/<text_id>")
@authorised()
def learn_next_redirect(user: chanza.database.User, text_id):
    with DB:
        chunk = DB.get_next_chunk_for(user.id, text_id)
        if not chunk:
            return bottle.redirect("/")
        return bottle.redirect(f"/learn/{chunk.id}")


@bottle.route("/learn/<chunk_id>")
@authorised()
def learn_page(user: chanza.database.User, chunk_id):
    chunk = DB.get_chunk(user.id, chunk_id)
    if not chunk:
        return page_template(user, "404 Learn Chunk", "<h1>404 chunk not found</h1>")
    text = DB.get_text(user.id, chunk.text_id)

    lines = chunk.slice.select(text.text)

    return page_template(
        user,
        f"Learn Chunk {text.name} C{chunk.sequence_number + 1}",
        """
        <h1>Learn {{name}} C{{seq}}</h1>
        <script>window.chunk_data = JSON.parse({{!lines}});</script>
        <div class="spacer"></div>
        <div class="pager"></div>
        <div class="formatted-text learn"></div>
        <div class="spacer"></div>
        <div class="action-buttons">
            <form method="post" action="/learn/save/{{chunk_id}}">
                <input type="submit" value="Save">
            </form>
        </div>
        """,
        chunk_id=chunk_id,
        name=text.name,
        seq=chunk.sequence_number + 1,
        lines=json.dumps(json.dumps(lines)),
    )


@bottle.route("/learn/save/<chunk_id>", method="post")
@authorised()
def save_learning_action(user: chanza.database.User, chunk_id):
    chunk = DB.get_chunk(user.id, chunk_id)
    if not chunk:
        return page_template(user, "404 Learn Chunk", "<h1>404 chunk not found</h1>")

    srs_state = chanza.srs.generate_initial_srs_state()
    with DB:
        DB.save_chunk_srs(user.id, chunk_id, srs_state)

    return bottle.redirect(f"/")


@bottle.route("/practice/<chunk_id>")
@authorised()
def practice_response_page(user: chanza.database.User, chunk_id):
    chunk = DB.get_chunk(user.id, chunk_id)
    if not chunk:
        return page_template(user, "404 Practice Chunk", "<h1>404 chunk not found</h1>")
    text = DB.get_text(user.id, chunk.text_id)

    lines = chunk.slice.select(text.text)
    formatted_text = "<br>".join(lines)

    return page_template(
        user,
        f"Practice Chunk {text.name} C{chunk.sequence_number + 1}",
        """
        <h1>Practice {{name}} C{{seq}}</h1>
        <div class="spacer"></div>
        <div class="formatted-text practice">{{!formatted_text}}</div>
        <div class="spacer"></div>
        <div class="action-buttons">
            <form method="post" action="/practice/save/{{chunk_id}}">
                <input type="submit" name="action" value="Success">
                <input type="submit" name="action" value="Couldn't Remember">
            </form>
        </div>
        """,
        chunk_id=chunk_id,
        name=text.name,
        seq=chunk.sequence_number + 1,
        formatted_text=formatted_text,
    )


@bottle.route("/practice/save/<chunk_id>", method="post")
@authorised()
def save_practice_action(user: chanza.database.User, chunk_id):
    chunk = DB.get_chunk(user.id, chunk_id)
    if not chunk:
        return page_template(user, "404 Practice Chunk", "<h1>404 chunk not found</h1>")
    if not chunk.srs_state:
        return page_template(
            user, "404 Practice Chunk", "<h1>404 chunk not in scheduler</h1>"
        )

    params = bottle.request.forms.decode()
    action: str = params.get("action")

    srs_state = chanza.srs.update_state(chunk.srs_state, good=action == "Success")
    with DB:
        DB.save_chunk_srs(user.id, chunk_id, srs_state)

    return bottle.redirect("/practice/next")


@bottle.route("/familiarity")
@authorised()
def familiarity_pipeline_page(user: chanza.database.User):
    PIPELINE_DEPTH = 5

    texts = DB.get_texts(
        user.id,
        chanza.database.TextQuery(
            familiarity_active=True,
            sort=chanza.database.TextSort.NAME_ORDER,
        ),
    )
    sections = []
    for text in texts:
        slices = [
            chunk.slice
            for chunk in DB.get_chunks(
                user.id,
                chanza.database.ChunkQuery(
                    text_id=text.id,
                    state=chanza.database.ChunkState.INACTIVE,
                    sort=chanza.database.ChunkSort.SEQUENCED_ORDER,
                    limit=PIPELINE_DEPTH,
                ),
            )
        ]
        if not slices:
            continue

        contiguous_slices: Iterable[Slice] = []
        current = slices[0]
        for slice in slices:
            if current.intersects(slice):
                current = current.union(slice)
            else:
                contiguous_slices.append(current)
                current = slice
        contiguous_slices.append(current)

        formatted_slices: Iterable[str] = []
        for slice in contiguous_slices:
            formatted_slices.append(
                # annoyingly our format function adds a newline
                "".join(
                    format("{{line}}", line=line) for line in slice.select(text.text)
                ).rstrip()
            )

        section = format(
            """
            <h2>{{title}}</h2>
            <div class="formatted-text">{{!formatted_slices}}</div>
            """,
            title=text.name,
            formatted_slices="\n\n------\n\n".join(formatted_slices),
        )
        sections.append(section)

    return page_template(
        user,
        "",
        """
        <h1>Familiarity Pipeline</h1>
        {{!sections}}
        """,
        sections="\n".join(sections),
    )


def format(content: str, **kwargs) -> str:
    if "\n" not in content:
        content += "\n"
    return bottle.template(content, **kwargs)


def page_template(
    user: chanza.database.User, title: str, content: str, **kwargs
) -> str:
    practice_count = len(DB.get_halflife_chunks(user.id, limit=10))

    with open(os.path.join(os.path.dirname(__file__), "script.js")) as fp:
        script = fp.read()
    with open(os.path.join(os.path.dirname(__file__), "style.css")) as fp:
        styles = fp.read()
    return format(
        """
        <!doctype html>
        <html>
        <head>
            <title>{{title}}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
        </head>
        <body>
            <nav>
                <a href="/">Dashboard</a>
                <a href="/me">Profile</a>
                <a href="/add">Add</a>
                <a href="/practice/next">Practice ({{practice_count}})</a>
                <a href="/familiarity">Familiarity Pipeline</a>
            </nav>

            {{!inner}}

            <footer>
            Chanza {{version}}
            </footer>

            <style type="text/css">{{!styles}}</style>
            <script type="text/javascript">{{!script}}</script>
        <body>
        </html>
        """.strip(),
        title=title + " — Chanza" if title else "Chanza",
        practice_count=(
            "none"
            if practice_count == 0
            else "+9"
            if practice_count > 9
            else practice_count
        ),
        inner=format(content, **kwargs),
        version=(
            f"{chanza.buildstamp.commit_sha}-{chanza.buildstamp.build_type} "
            f"({chanza.buildstamp.built_at})"
        ),
        script=script,
        styles=styles,
    )


def load_user_from_header(auth_header):
    if not auth_header.startswith("Basic "):
        return None

    user_password_b64 = auth_header[6:]
    user_password = base64.b64decode(user_password_b64).decode("utf-8")
    if ":" not in user_password:
        return None

    username, password = user_password.split(":", 1)
    user = DB.get_user(username)

    if not user:
        # we assume they passed the actual auth gate and so make them a record
        with DB:
            user = DB.create_user(
                username=username,
            )

    return user
