"use strict";

function init() {
    if (document.querySelector(".raw_text")) initTextEdit();
    if (document.querySelector(".learn")) initLearnPage();
    if (document.querySelector(".practice")) initPracticePage();
}

function initTextEdit() {
    const rawTextField = document.querySelector(".raw_text");
    if (!rawTextField) return;

    const joinBtn = document.querySelector("#btn-join");
    joinBtn.addEventListener("click", e => {
        e.preventDefault();

        const lines = rawTextField.value.split("\n");
        let text = "";
        for (let line of lines) {
            line = line.trim();
            if (line === "") {
                text += "\n\n";
            } else {
                if (text != "" && !text.endsWith("\n")) text += " ";
                text += line;
            }
        }
        rawTextField.value = text;

        return false;
    });

    const splitPunctuationBtn = document.querySelector("#btn-split-punctuation");
    splitPunctuationBtn.addEventListener("click", e => {
        e.preventDefault();

        // find opening quotes at the start or after a space,
        // OR find any amount of closing punctuation with a trailing space/EOF
        const SPLIT_REGEX = /((?:,? )[“]+|[.!?"”]+(?: |$))/;

        let lines = rawTextField.value.split("\n");
        let newLines = [];
        for (let line of lines) {
            line = line.trim();
            if (wc(line) > 8) {
                const sentences = line.split(SPLIT_REGEX);
                newLines.push(...mergeSplit(sentences));
            } else {
                newLines.push(line);
            }
        }
        lines = newLines;

        newLines = [];
        for (let line of lines) {
            line = line.trim();
            if (wc(line) > 8) {
                const phrases = line.split(/([,;:]) ?/);
                newLines.push(...mergeSplit(phrases));
            } else {
                newLines.push(line);
            }
        }
        lines = newLines;

        rawTextField.value = lines.join("\n").replaceAll("\n\n\n", "\n\n");
        return false;
    });
}

function initLearnPage() {
    const learnCard = document.querySelector(".learn");
    const pagerEl = document.querySelector(".pager");
    if (!learnCard) return;
    if (!window.chunk_data) return;

    const lines = window.chunk_data;
    const slides = [];

    // add an initial slide with all the lines
    slides.push(lines.map((_, i) => i));

    function* generateSpan(span, length, flip) {
        const modulus = length % span;
        let start = 0;
        if (flip && modulus > 0) {
            yield [0, modulus];
            start = modulus;
        }
        while (start < length) {
            const end = Math.min(length, start + span);
            yield [start, end];
            start = end;
        }
    }

    // a increasing spans of lines
    const maxSpan = Math.min(6, lines.length - 1); // at least one less than the total lines
    for (let span = 1; span <= maxSpan; span++) {
        // have the "overflow" at the start for odd spans
        const isOdd = span % 2 == 1;
        for (let [start, end] of generateSpan(span, lines.length, isOdd)) {
            const slide = [];
            for (let i = start; i < end; i++) slide.push(i);
            slides.push(slide);
        }
    }

    // and repeat the whole thing
    slides.push(slides[0]);

    let slideIndex = 0;
    function render() {
        if (slideIndex >= slides.length) {
            learnCard.innerHTML = "All done!";
            pagerEl.innerHTML = `${slides.length}/${slides.length}`;
        } else {
            const slide = slides[slideIndex];
            const text = slide.map(i => escapeHtml(lines[i])).join("<br>");
            learnCard.innerHTML = text;
            pagerEl.innerHTML = `${slideIndex+1}/${slides.length}`;
        }
    }

    document.addEventListener("click", e => {
        if (elementIn(e.target, "nav") || elementIn(e.target, ".action-buttons")) {
            return;
        }

        if (slideIndex > slides.length) return;
        e.preventDefault();

        slideIndex++;
        render();
    });

    render();
}

function initPracticePage() {
    const practiceCard = document.querySelector(".practice");
    if (!practiceCard) return;

    const original = practiceCard.innerHTML;
    const lines = original.split("<br>");

    // find which lines to show from the start and end
    let firstCount = Math.min(2, lines.length);
    while (firstCount < lines.length && lines[firstCount] === "") {
        firstCount++;
    }
    let lastCount = Math.min(1, lines.length);
    while (lastCount < lines.length && lines[lines.length - lastCount] === "") {
        lastCount++;
    }

    const shownLines = [
        ...lines.slice(0, firstCount),
        "...",
        ...lines.slice(-lastCount),
    ];
    const shown = shownLines.join("<br>");
    practiceCard.innerHTML = shown;

    document.addEventListener("click", e => {
        if (elementIn(e.target, "nav") || elementIn(e.target, ".action-buttons")) {
            return;
        }

        practiceCard.innerHTML = original;
    });
}

function wc(str) {
    return str.split(" ").length;
}

function mergeSplit(strs) {
    const lines = [];
    for (let i = 0; i < strs.length; i += 2) {
        const a = strs[i];
        const b = strs[i + 1];
        if (b !== undefined) {
            lines.push(a + b);
        } else {
            lines.push(a);
        }
    }
    return lines;
}

function escapeHtml(str) {
    return new Option(str).innerHTML;
}

function elementIn(el, selector) {
    while (el) {
        if (el.matches(selector)) return true;
        el = el.parentElement;
    }
    return false;
}

init();
