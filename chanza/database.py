from datetime import datetime
from enum import Enum
import json
import os.path
from typing import Iterable, NamedTuple, Optional
import sqlite3
from zoneinfo import ZoneInfo

from chanza.data import ParsedText, Slice, get_now
from chanza.srs import calculate_accurate_probability, refresh_probability, SrsState


class User(NamedTuple):
    id: int
    username: str
    email: Optional[str]
    timezone: ZoneInfo
    last_reported_at: Optional[datetime]


class Text(NamedTuple):
    id: int
    user_id: int
    name: str
    text: ParsedText
    line_count: int
    chunk_length: int
    familiarity_active: bool


class TextSummary(NamedTuple):
    id: int
    user_id: int
    name: str
    line_count: int
    chunk_length: int
    familiarity_active: bool
    chunk_count: int
    circulating_chunk_count: int


class TextSort(Enum):
    NAME_ORDER = "name"


class TextQuery(NamedTuple):
    familiarity_active: Optional[bool] = None
    sort: TextSort = TextSort.NAME_ORDER
    limit: int = 1000


class Chunk(NamedTuple):
    id: int
    user_id: int
    text_id: int
    slice: Slice
    sequence_number: int
    srs_state: Optional[SrsState]


class ChunkState(Enum):
    ACTIVE = "active"
    INACTIVE = "inactive"


class ChunkSort(Enum):
    TEXT_ORDER = "text"
    SEQUENCED_ORDER = "sequenced"
    WEAKEST_FIRST = "weakest-first"


class ChunkQuery(NamedTuple):
    chunk_id: Optional[int] = None
    text_id: Optional[int] = None
    state: Optional[ChunkState] = None
    sort: ChunkSort = ChunkSort.TEXT_ORDER
    limit: int = 1000


class Database:
    def __init__(self, db: sqlite3.Connection) -> None:
        self.db = db

    def __del__(self):
        self.db.close()
        self.db = None

    def __enter__(self):
        self.db.execute("BEGIN IMMEDIATE;")

    def __exit__(self, exc_type, exc_value, traceback) -> bool:
        if exc_type:
            self.db.rollback()
        else:
            self.db.commit()
        return False  # continue exception handling

    def backup_to(self, fp):
        for line in self.db.iterdump():
            fp.write(line.encode("utf-8"))

    def get_user(self, username) -> User:
        row = self.db.execute(
            """
            SELECT id, username, email, timezone, last_reported_at
            FROM user
            WHERE username = :username;
            """,
            {"username": username},
        ).fetchone()
        if not row:
            return None

        return User(
            id=row[0],
            username=row[1],
            email=row[2],
            timezone=ZoneInfo(row[3]),
            last_reported_at=datetime.fromisoformat(row[4]) if row[4] else None,
        )

    def list_users(self) -> Iterable[User]:
        for row in self.db.execute(
            """
            SELECT id, username, email, timezone, last_reported_at
            FROM user;
            """,
        ):
            yield User(
                id=row[0],
                username=row[1],
                email=row[2],
                timezone=ZoneInfo(row[3]),
                last_reported_at=datetime.fromisoformat(row[4]) if row[4] else None,
            )

    def create_user(
        self,
        username: str,
        email: str = None,
        timezone: ZoneInfo = None,
    ) -> User:
        if not timezone:
            timezone = ZoneInfo("Australia/Adelaide")

        row = self.db.execute(
            """
            INSERT INTO user (username, email, timezone)
            VALUES (:username, :email, :timezone)
            RETURNING id;
            """,
            {
                "username": username,
                "email": email,
                "timezone": timezone.key,
            },
        ).fetchone()
        if not row:
            raise Exception("Failed to save text record")
        user_id = row[0]

        return User(
            id=user_id,
            username=username,
            email=email,
            timezone=timezone,
            last_reported_at=None,
        )

    def update_user(
        self,
        id: int,
        email: Optional[str],
        timezone: ZoneInfo,
        last_reported_at: Optional[datetime],
    ) -> User:
        row = self.db.execute(
            """
            UPDATE user
            SET email = :email,
                timezone = :timezone,
                last_reported_at = :last_reported_at
            WHERE id = :id;
            """,
            {
                "id": id,
                "email": email,
                "timezone": timezone.key if timezone else None,
                "last_reported_at": last_reported_at.isoformat("T", "seconds")
                if last_reported_at
                else None,
            },
        )

    def save_text(
        self,
        user_id: int,
        name: str,
        text: ParsedText,
        chunk_length: int,
        chunks: Iterable[Slice],
        sequence: Iterable[int],
    ) -> int:
        row = self.db.execute(
            """
            INSERT INTO text (user_id, name, raw_text, chunk_length, line_count,
                              created_at, updated_at)
            VALUES (:user_id, :name, :raw, :chunk_length, :line_count, :now, :now)
            RETURNING id;
            """,
            {
                "user_id": user_id,
                "name": name,
                "raw": str(text),
                "chunk_length": chunk_length,
                "line_count": len(text.lines),
                "now": get_now().isoformat("T", "seconds"),
            },
        ).fetchone()
        if not row:
            raise Exception("Failed to save text record")
        text_id = row[0]

        for chunk, index in zip(chunks, sequence):
            self.db.execute(
                """
                INSERT INTO chunk (user_id, text_id, range_first, range_last, sequence_number)
                VALUES (:user_id, :text_id, :first, :last, :sequence_number);
                """,
                {
                    "user_id": user_id,
                    "text_id": text_id,
                    "first": chunk.first,
                    "last": chunk.last,
                    "sequence_number": index,
                },
            )

        return text_id

    def update_text(
        self,
        id: int,
        user_id: int,
        name: str,
        text: ParsedText,
        chunk_length: int,
        chunks: Iterable[Slice],
        sequence: Iterable[int],
    ) -> None:
        row = self.db.execute(
            """
            UPDATE text
            SET name = :name,
                raw_text = :raw_text,
                chunk_length = :chunk_length,
                line_count = :line_count,
                updated_at = :now
            WHERE id = :id AND user_id = :user_id;
            """,
            {
                "id": id,
                "user_id": user_id,
                "name": name,
                "raw_text": str(text),
                "chunk_length": chunk_length,
                "line_count": len(text.lines),
                "now": get_now().isoformat("T", "seconds"),
            },
        )

        self.db.execute("DELETE FROM chunk WHERE text_id = :id;", {"id": id})
        for chunk, index in zip(chunks, sequence):
            self.db.execute(
                """
                INSERT INTO chunk (user_id, text_id, range_first, range_last, sequence_number)
                VALUES (:user_id, :text_id, :first, :last, :sequence_number);
                """,
                {
                    "user_id": user_id,
                    "text_id": id,
                    "first": chunk.first,
                    "last": chunk.last,
                    "sequence_number": index,
                },
            )

    def update_text_familiarity(
        self,
        id: int,
        user_id: int,
        familiarity_active: bool,
    ) -> None:
        self.db.execute(
            """
            UPDATE text
            SET familiarity_active = :familiarity_active,
                updated_at = :now
            WHERE id = :id AND user_id = :user_id;
            """,
            {
                "id": id,
                "user_id": user_id,
                "familiarity_active": familiarity_active,
                "now": get_now().isoformat("T", "seconds"),
            },
        )

    def get_text(self, user_id: int, id: int) -> Optional[Text]:
        row = self.db.execute(
            """
            SELECT id, user_id, name, raw_text, line_count, chunk_length,
                   familiarity_active
            FROM text
            WHERE id = :id AND user_id = :user_id;
            """,
            {"id": id, "user_id": user_id},
        ).fetchone()
        if not row:
            return None

        return Text(
            id=row[0],
            user_id=row[1],
            name=row[2],
            text=ParsedText.parse(row[3]),
            line_count=row[4],
            chunk_length=row[5],
            familiarity_active=row[6] != 0,
        )

    def get_texts(self, user_id: int, query: TextQuery) -> Iterable[Text]:
        query_params = {"user_id": user_id}

        where_pieces = ["user_id = :user_id"]
        if query.familiarity_active is not None:
            if query.familiarity_active:
                where_pieces.append(f"familiarity_active")
            else:
                where_pieces.append(f"NOT familiarity_active")
        if where_pieces:
            where = "WHERE " + (" AND ".join(where_pieces))

        if query.sort == TextSort.NAME_ORDER:
            order_by = "name ASC"
        else:
            raise ValueError(f"Unknown text sort: {query.sort}")

        if isinstance(query.limit, int):
            limit = f"LIMIT {query.limit}"
        else:
            limit = ""

        for row in self.db.execute(
            f"""
            SELECT id, user_id, name, raw_text, line_count, chunk_length,
                   familiarity_active
            FROM text
            {where}
            ORDER BY {order_by}
            {limit};
            """,
            query_params,
        ):
            yield Text(
                id=row[0],
                user_id=row[1],
                name=row[2],
                text=ParsedText.parse(row[3]),
                line_count=row[4],
                chunk_length=row[5],
                familiarity_active=row[6] != 0,
            )

    def get_chunks(self, user_id: int, query: ChunkQuery) -> Iterable[Chunk]:
        query_params = {"user_id": user_id}

        where_pieces = ["user_id = :user_id"]
        if query.chunk_id:
            query_params["chunk_id"] = query.chunk_id
            where_pieces.append("chunk.id = :chunk_id")
        if query.text_id:
            query_params["text_id"] = query.text_id
            where_pieces.append("chunk.text_id = :text_id")
        if query.state:
            if query.state == ChunkState.ACTIVE:
                where_pieces.append("chunk.recall_probability IS NOT NULL")
            elif query.state == ChunkState.INACTIVE:
                where_pieces.append("chunk.recall_probability IS NULL")
            else:
                raise Exception(f"unsupported chunk state: {query.state}")
        if where_pieces:
            where = "WHERE " + (" AND ".join(where_pieces))

        if query.sort == ChunkSort.TEXT_ORDER:
            order_by = "chunk.id ASC"
        elif query.sort == ChunkSort.SEQUENCED_ORDER:
            order_by = "chunk.sequence_number ASC"
        elif query.sort == ChunkSort.WEAKEST_FIRST:
            order_by = "chunk.recall_probability ASC"
        else:
            raise Exception(f"unsupported sort: {query.sort}")

        if isinstance(query.limit, int):
            limit = f"LIMIT {query.limit}"
        else:
            limit = ""

        for row in self.db.execute(
            f"""
            SELECT id, user_id, text_id, range_first, range_last,
                   sequence_number,
                   ebisu, recall_probability, last_practiced
            FROM chunk
            {where}
            ORDER BY {order_by}
            {limit};
            """,
            query_params,
        ):
            if row[6] and row[7] is not None and row[8]:
                srs_state = SrsState(
                    json.loads(row[6]), row[7], datetime.fromisoformat(row[8])
                )
            else:
                srs_state = None

            yield Chunk(
                id=row[0],
                user_id=row[1],
                text_id=row[2],
                slice=Slice(row[3], row[4]),
                sequence_number=row[5],
                srs_state=srs_state,
            )

    def get_chunks_for_text(self, user_id: int, text_id: int) -> Iterable[Chunk]:
        return self.get_chunks(user_id, ChunkQuery(text_id=text_id))

    def get_chunk(self, user_id: int, chunk_id: int) -> Chunk:
        chunks = list(self.get_chunks(user_id, ChunkQuery(chunk_id=chunk_id, limit=1)))
        return chunks[0] if chunks else None

    def get_next_chunk_for(self, user_id: int, text_id: int) -> Chunk:
        chunks = list(
            self.get_chunks(
                user_id,
                ChunkQuery(
                    text_id=text_id,
                    state=ChunkState.INACTIVE,
                    sort=ChunkSort.SEQUENCED_ORDER,
                    limit=1,
                ),
            )
        )
        return chunks[0] if chunks else None

    def get_weakest_chunk(self, user_id: int) -> Chunk:
        chunks = list(
            self.get_chunks(
                user_id,
                ChunkQuery(
                    state=ChunkState.ACTIVE, sort=ChunkSort.WEAKEST_FIRST, limit=1
                ),
            )
        )
        return chunks[0] if chunks else None

    def get_halflife_chunks(self, user_id: int, limit: int) -> Iterable[Chunk]:
        def recall_probability(chunk: Chunk):
            return calculate_accurate_probability(chunk.srs_state)

        return [
            chunk
            for chunk in self.get_chunks(
                user_id,
                ChunkQuery(
                    state=ChunkState.ACTIVE, sort=ChunkSort.WEAKEST_FIRST, limit=limit
                ),
            )
            if recall_probability(chunk) <= 0.5
        ]

    def save_chunk_srs(
        self, user_id: int, chunk_id: int, srs_state: Optional[SrsState]
    ):
        if srs_state:
            self.db.execute(
                """
                UPDATE chunk
                SET ebisu = :e,
                    recall_probability = :rp,
                    last_practiced = :lp
                WHERE id = :id AND user_id = :user_id;
                """,
                {
                    "id": chunk_id,
                    "user_id": user_id,
                    "e": json.dumps(srs_state.ebisu),
                    "rp": srs_state.recall_probability,
                    "lp": srs_state.last_practiced.isoformat("T", "seconds"),
                },
            )
        else:
            self.db.execute(
                """
                UPDATE chunk
                SET ebisu = NULL,
                    recall_probability = NULL
                WHERE id = :id AND user_id = :user_id;
                """,
                {
                    "id": chunk_id,
                    "user_id": user_id,
                },
            )

    def refresh_probabilities(self):
        for row in self.db.execute(
            """
            SELECT id, ebisu, recall_probability, last_practiced
            FROM chunk
            WHERE ebisu IS NOT NULL
              AND last_practiced is not NULL
            ORDER BY id;
            """,
        ):
            id = row[0]
            srs_state = SrsState(
                json.loads(row[1]), row[2], datetime.fromisoformat(row[3])
            )
            new_probability = refresh_probability(srs_state)
            self.db.execute(
                "UPDATE chunk SET recall_probability = :rp WHERE id = :id;",
                {"id": id, "rp": new_probability},
            )

    def get_texts_summary(self, user_id: int) -> Iterable[TextSummary]:
        for row in self.db.execute(
            """
            SELECT text.id, text.user_id, name, line_count,
                   chunk_length, familiarity_active,
                   COUNT(chunk.id), COUNT(chunk.ebisu)
            FROM text
            LEFT JOIN chunk ON chunk.text_id = text.id
            WHERE text.user_id = :user_id
            GROUP BY text.id
            ORDER BY familiarity_active DESC,
                     COUNT(chunk.ebisu) * 1.0 / COUNT(chunk.id) DESC,
                     text.name ASC;
            """,
            {"user_id": user_id},
        ):
            yield TextSummary(
                id=row[0],
                user_id=row[1],
                name=row[2],
                line_count=row[3],
                chunk_length=row[4],
                familiarity_active=row[5],
                chunk_count=row[6],
                circulating_chunk_count=row[7],
            )


def open_connection(db_path: str) -> Database:
    db = sqlite3.connect(os.path.join(db_path, "chanza.db"))
    apply_migrations(db)
    return Database(db)


def apply_migrations(db: sqlite3.Connection):
    db.execute(
        """
        CREATE TABLE IF NOT EXISTS version (
            initial INTEGER NOT NULL,
            current INTEGER NOT NULL
        );
        """
    )

    db.execute("BEGIN IMMEDIATE;")

    try:
        result = db.execute("SELECT current FROM version;").fetchone()
        if not result:
            print("[db] init empty database")
            db.execute("INSERT INTO version (initial, current) VALUES (0, 0);")
            version = 0
        else:
            version = result[0]

        print(f"[db] at schema version {version}")

        if version < 1:
            print("[db] applying patch 1 - texts and chunks")
            db.execute(
                """
                CREATE TABLE text (
                    id INTEGER PRIMARY KEY,
                    name TEXT NOT NULL,
                    sequence_number INTEGER NOT NULL,
                    line_count INTEGER NOT NULL,
                    raw_text TEXT NOT NULL,
                    active BOOLEAN DEFAULT 1 NOT NULL,
                    chunk_length INTEGER NOT NULL,
                    UNIQUE (sequence_number)
                );
                """
            )
            db.execute(
                """
                CREATE TABLE chunk (
                    id INTEGER PRIMARY KEY,
                    text_id INTEGER REFERENCES text(id),
                    sequence_number INTEGER NOT NULL,
                    range_first INTEGER NOT NULL,
                    range_last INTEGER NOT NULL,
                    UNIQUE (text_id, sequence_number)
                );
                """
            )
            version = 1

        if version < 2:
            print("[db] applying patch 2 - chunk SRS")
            db.execute(
                """
                ALTER TABLE chunk
                ADD COLUMN ebisu TEXT DEFAULT NULL;
                """
            )
            db.execute(
                """
                ALTER TABLE chunk
                ADD COLUMN recall_probability REAL DEFAULT NULL;
                """
            )
            db.execute(
                """
                ALTER TABLE chunk
                ADD COLUMN last_practiced TEXT DEFAULT NULL;
                """
            )
            version = 2

        if version < 3:
            print("[db] applying patch 3 - users")
            db.execute(
                """
                CREATE TABLE user (
                    id INTEGER PRIMARY KEY,
                    username TEXT,
                    UNIQUE (username)
                );
                """
            )
            # add a default user
            db.execute(
                """
                INSERT INTO user (username) VALUES ("lachlan");
                """
            )
            version = 3

        if version < 4:
            print("[db] applying patch 4 - users for texts and chunks")
            db.execute(
                """
                ALTER TABLE text
                ADD COLUMN user_id INTEGER REFERENCES user(id);
                """
            )
            db.execute(
                """
                ALTER TABLE chunk
                ADD COLUMN user_id INTEGER REFERENCES user(id);
                """
            )
            # assign everything to the default user
            db.execute(
                """
                UPDATE text SET user_id = 1;
                """
            )
            db.execute(
                """
                UPDATE chunk SET user_id = 1;
                """
            )
            version = 4

        if version < 5:
            print("[db] applying patch 5 - email stuff for users")
            db.execute(
                """
                ALTER TABLE user
                ADD COLUMN email TEXT;
                """
            )
            db.execute(
                """
                ALTER TABLE user
                ADD COLUMN timezone TEXT;
                """
            )
            db.execute(
                """
                ALTER TABLE user
                ADD COLUMN last_reported_at TEXT;
                """
            )
            # assign a default timezone
            db.execute(
                """
                UPDATE user SET timezone = "Australia/Adelaide";
                """
            )
            version = 5

        if version < 6:
            print("[db] applying patch 6 - drop text seq# and active state")
            db.execute("PRAGMA foreign_keys=OFF;")
            db.execute(
                """
                CREATE TABLE text_tmp (
                    id INTEGER PRIMARY KEY,
                    user_id INTEGER REFERENCES user(id),
                    name TEXT NOT NULL,
                    line_count INTEGER NOT NULL,
                    raw_text TEXT NOT NULL,
                    chunk_length INTEGER NOT NULL
                );
                """
            )
            db.execute(
                """
                INSERT INTO text_tmp (id, user_id, name, line_count, raw_text, chunk_length)
                SELECT id, user_id, name, line_count, raw_text, chunk_length
                FROM text;
                """
            )
            db.execute("DROP TABLE text;")
            db.execute("ALTER TABLE text_tmp RENAME TO text;")
            db.execute("PRAGMA foreign_key_check;")
            db.execute("PRAGMA foreign_keys=ON;")
            db.execute("CREATE INDEX text_name ON text (name);")
            version = 6

        if version < 7:
            print("[db] applying patch 7 - text created/updated at")
            db.execute("ALTER TABLE text ADD COLUMN created_at TEXT;")
            db.execute("ALTER TABLE text ADD COLUMN updated_at TEXT;")
            db.execute(
                """
                UPDATE text SET created_at = :now,
                                updated_at = :now;
                """,
                {"now": get_now().isoformat("T", "seconds")},
            )
            db.execute("CREATE INDEX text_created_at ON text (created_at);")
            db.execute("CREATE INDEX text_updated_at ON text (updated_at);")
            version = 7

        if version < 8:
            print("[db] applying patch 8 - familiarity pipelining active state")
            db.execute("ALTER TABLE text ADD COLUMN familiarity_active INT;")
            db.execute("UPDATE text SET familiarity_active = 0;")
            version = 8

        if version < 9:
            print("[db] applying patch 9 - raw line indexes to content indexes")
            chunk_count = 0
            for text_row in db.execute("SELECT id, raw_text from text;"):
                text_id = text_row[0]
                parsed = ParsedText.parse(text_row[1])

                old_mapping = {}
                new_mapping = {}
                old_index = 0
                new_index = 0
                for line in text_row[1].split("\n"):
                    old_mapping[old_index] = new_index
                    if line:
                        new_mapping[new_index] = old_index
                        new_index += 1
                    old_index += 1

                chunks = db.execute(
                    """
                    SELECT id, range_first, range_last
                    FROM chunk
                    WHERE text_id = :text_id;
                    """,
                    {"text_id": text_id},
                ).fetchall()
                for chunk_row in chunks:
                    id, first, last = chunk_row
                    new_first = old_mapping[first]
                    new_last = old_mapping[last]

                    # do some sanity checking
                    if new_mapping[new_first] != first:
                        # there might be change if the original line was blank
                        if new_mapping[new_first] > first:
                            if not parsed.lines[new_first - 1].endswith("\n"):
                                raise Exception(
                                    f"failed to map chunk {id} first {first} -> {new_first} != {new_mapping[new_first]}")
                        else:
                            raise Exception(
                                f"failed to map chunk {id} first {first} -> {new_first} != {new_mapping[new_first]}")
                    if new_mapping[new_last] != last:
                        raise Exception(
                            f"failed to map chunk {id} last {last} -> {new_last} != {new_mapping[new_last]}")

                    # update
                    db.execute(
                        """
                        UPDATE chunk
                        SET range_first = :first,
                            range_last = :last
                        WHERE id = :id;
                        """,
                        {
                            "id": id,
                            "first": new_first,
                            "last": new_last,
                        },
                    )
                    chunk_count += 1
            print(f"-> updated {chunk_count} chunks")
            version = 9

        if version < 10:
            print("[db] applying patch 10 - rm redundant trailing chunks")
            chunk_count = 0
            for text_row in db.execute("SELECT id from text;"):
                text_id = text_row[0]

                last_two_chunks = db.execute(
                    """
                    SELECT id, range_first, range_last
                    FROM chunk
                    WHERE text_id = :text_id
                    ORDER BY id DESC
                    LIMIT 2;
                    """,
                    {"text_id": text_id},
                ).fetchall()
                if len(last_two_chunks) != 2:
                    continue
                last_two_chunks.reverse()

                second_to_last_end = last_two_chunks[0][2]
                last_end = last_two_chunks[1][2]

                if second_to_last_end == last_end:
                    db.execute("DELETE FROM chunk WHERE id = :id;", {"id": last_two_chunks[1][0]})
                    chunk_count += 1

            print(f"-> removed {chunk_count} chunks")
            version = 10

        db.execute("UPDATE version SET current = ?;", (version,))
        db.commit()
    except Exception:
        db.rollback()
        raise
