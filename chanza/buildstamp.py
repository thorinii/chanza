from datetime import datetime, timezone
import os

build_type = os.environ.get("CHANZA_BUILD_TYPE", "debug")
debug = build_type == "debug"

# this will be overridden by the .gitlab-ci.yml script
built_at_raw = None
if built_at_raw:
    built_at = datetime.fromisoformat(built_at_raw)
else:
    built_at = datetime.now(tz=timezone.utc)

# this will also be overridden by the .gitlab-ci.yml script
commit_sha = None
